import { Component, OnInit } from '@angular/core';
import { UsersService } from '../../services/users.service'

import { User } from '../../models/User';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {

  users: User[];
  currentUser: User = {
    id: 0,
    name: '',
    phone: '',
    email: ''
  }

  isEdit: boolean = false;

  constructor(private userService: UsersService) { }

  ngOnInit(): void {
    this.userService.getUsers().subscribe(users =>{
      this.users = users;
    });
  }

  onNewUser(user: User){
    this.users.unshift(user);
  }

  editUser(user: User){
    this.currentUser = user;
    this.isEdit = true;
  }

  onUpdatedUser(user: User){
    this.users.forEach((cur, index) =>{
      if(user.id === cur.id){
        this.users.splice(index, 1);
        this.users.unshift(user);
        this.isEdit = false;
        this.currentUser = {
          id: 0,
          name: '',
          phone: '',
          email: ''
        }
      }
    })
  }

  removeUser(user: User){
    if(confirm('Are you sure?')){
      this.userService.removeUser(user.id).subscribe(() =>{
        this.users.forEach((cur, index) =>{
          if(user.id === cur.id){
            this.users.splice(index, 1);
          }
        })
      });
    }
  }

}
