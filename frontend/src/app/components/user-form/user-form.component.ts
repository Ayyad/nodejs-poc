import { Component, OnInit, EventEmitter, Output, Input } from '@angular/core';
import { UsersService } from '../../services/users.service'

import { User } from '../../models/User';

@Component({
  selector: 'app-user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.css']
})

export class UserFormComponent implements OnInit {
  @Output() newUser: EventEmitter<User> = new EventEmitter();
  @Output() updatedUser: EventEmitter<User> = new EventEmitter();
  @Input() currentUser: User;
  @Input() isEdit: boolean;


  loader: boolean = false;


  constructor(private userService: UsersService) { }

  ngOnInit(): void {
  }

  addUser(name, phone, email){
    this.loader = true;
    if(!name || !phone || !email){
      alert("Please enter data");
      this.loader = false;
    } else{
      this.userService.saveUser({name, phone, email} as User).subscribe(user => {
        this.newUser.emit(user);
        this.currentUser = {
          id: 0,
          name: '',
          phone: '',
          email: ''
        };
        this.loader = false;
      });
    }
  }

  updateUser() {
    this.loader = true;
    this.userService.updateUsers(this.currentUser).subscribe(user => {
      this.isEdit = false;
      this.updatedUser.emit(user);
      console.log(user);
      this.loader = false;
    });
  }

}
