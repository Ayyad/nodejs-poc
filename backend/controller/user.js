const User = require('../model/user');
const { validationResult } = require('express-validator/check');

exports.getUsers = (req, res, next) => {
    User.findAll()
    .then(users => {
        res.status(200).json({users: users})
    })
    .catch( err => {
        if(!err.statusCode){
            err.statusCode = 500;
        }
        next(err);
    });
};

exports.addUser = (req, res, next) => {
   
    const errors = validationResult(req);
    if (!errors.isEmpty()){
        const error = new Error(errors.array()[0].msg);
        error.statusCode = 442;
        throw error;
    }
    const name = req.body.name;
    const email = req.body.email;
    const mobile = req.body.mobile;

    const user = User.create({
        name: name,
        email: email,
        mobile: mobile,
    })
    .then( result => {
        res.status(201).json({
            message: 'user created!',
            post: result
        });
    })
    .catch(err => {
        if(!err.statusCode){
            err.statusCode = 500;
        }
        next(err);
    })

};

exports.editUser = (req, res, next) => {
    const userId  = req.params.userId;
    const errors = validationResult(req);
    if (!errors.isEmpty()){
        const error = new Error(errors.array()[0].msg);
        error.statusCode = 442;
        throw error;
    }
    const name = req.body.name;
    const email = req.body.email;
    const mobile = req.body.mobile;

    return User.findByPk(userId)
    .then(user => {
        if(!user){
            const error = new Error('Cannot find the user');
            error.statusCode = 442;
            throw error;
        }else{
            user.name = name;
            user.email = email;
            user.mobile = mobile;
            return user.save()
            .then(result => {
                res.status(200).json({
                    message: 'Editing has been saved successfully !'
                });
            })      
        }
    })
    .catch(err => {
        if(!err.statusCode){
            err.statusCode = 500;
        }
        next(err);
    })
}

exports.deleteUser= (req, res, next) => {
    const userId  = req.params.userId;
    console.log(userId);
    return User.findByPk(userId)
    .then(user => {
        if(!user){
            const error = new Error('Cannot find the user');
            error.statusCode = 442;
            throw error;
        }else{
            user.destroy();
            res.status(200).json({
                message: 'user deleted!'
            });
        }
    })
    .catch(err => {
        if(!err.statusCode){
            err.statusCode = 500;
        }
        next(err);
    })
};




// exports.validateAddUser= (req, res, next) => {
//     console.log('validate');
//     return [
//         body('name').trim().isLength({min:2}),
//         body('email').trim().isLength({min:3}),
//         body('mobile').trim().isNumeric()
//     ]
// }
