const Sequelize = require('sequelize');

const sequelize = new Sequelize('nodejs_poc', 'root', 'sql', {
    dialect: 'mysql',
    host: 'localhost',
    logging: false,
});

module.exports = sequelize;
