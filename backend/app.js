const express = require('express');
const bodyParser = require('body-parser');

const sequelize = require('./util/database');

const userRoutes  = require('./routes/user');

const User = require('./model/user');

const app = express();


app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'Content-Type, Authorization');
    next();
});

app.use(bodyParser.json());

app.use('/api/v1',userRoutes);

app.use((error, req, res, next) => {
    console.log('error',error);
    const status = error.statusCode;
    const message = error.message;
    res.status(status).json({message: message});
});

sequelize
// .sync({force: true})
.sync()
.then(result => {
    app.listen(8000);
})
.catch(err => {console.log(err)});
