-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: May 09, 2020 at 11:17 PM
-- Server version: 5.7.29-0ubuntu0.18.04.1
-- PHP Version: 7.2.24-0ubuntu0.18.04.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `nodejs_poc`
--

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `mobile` varchar(255) NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `mobile`, `createdAt`, `updatedAt`) VALUES
(3, 'sssss', 'aaaaaaaaaaaa', 'sssssssssssssssssss', '2020-05-08 23:56:35', '2020-05-08 23:56:35'),
(4, 'yaseeen', 'Mohamed@ayyad.com', '123452444444444', '2020-05-08 23:56:48', '2020-05-09 02:03:29'),
(5, 'new user', 'test@test.test', '12345678922', '2020-05-09 00:18:27', '2020-05-09 00:18:27'),
(6, 'new user', '@testsssssssssssss', '12345678922', '2020-05-09 00:18:41', '2020-05-09 00:18:41'),
(7, 'new user', '@mohamed ayyad', '12345678922', '2020-05-09 00:19:24', '2020-05-09 00:19:24'),
(8, 'your name', 'Mohamed@ayyad.com', '123452444444444', '2020-05-09 00:24:46', '2020-05-09 00:24:46'),
(9, 'your name', 'Mohamed@ayyad.com', '123452444444444', '2020-05-09 00:28:17', '2020-05-09 00:28:17'),
(10, 'y', 'Mohamed@ayyad.com', '123452444444444', '2020-05-09 00:28:23', '2020-05-09 00:28:23'),
(11, 'yaseen', 'Mohamed@ayyad.com', '123452444444444', '2020-05-09 00:33:34', '2020-05-09 00:33:34');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
