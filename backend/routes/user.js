const express = require('express');
const router = express.Router();
const userController = require('../controller/user');
const { check, validationResult } = require('express-validator/check');

router.get('/users', userController.getUsers);

router.post('/user', [
    check('name').trim().isLength({min:2}).withMessage('Your Name must be exceed 2 characters'),
    check('email').trim().isLength({min:3}).isEmail().withMessage('Invalid Email'),
    check('mobile').trim().isLength({min:9}).withMessage('Mobile number must exceed 9 numbers'),
],
 userController.addUser);

router.put('/user/:userId', [
    check('name').trim().isLength({min:2}).withMessage('Your Name must be exceed 2 characters'),
    check('email').trim().isLength({min:3}).isEmail().withMessage('Invalid Email'),
    check('mobile').trim().isLength({min:9}).withMessage('Mobile number must exceed 9 numbers'),
],
userController.editUser);

router.delete('/user/:userId', userController.deleteUser);

module.exports = router;
